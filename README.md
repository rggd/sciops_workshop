# SciOps Workshop

This repository contains the slides for workshops on
[SciOps](https://gitlab.com/rggd/SciOps).

## Contributing

Feel free to improve the slides in accordance with the project's
GPL-3.0-only license.

### GPL-3.0-only

**Please note that, even if specified otherwise in the GitLab UI, this
project is licensed under the GPL-3.0-only.** For more details please
read [the rationale for this
decision](https://gitlab.com/rggd/SciOps/-/commit/3a79052244b3e6a42459eadca5699601de89f5b6).
