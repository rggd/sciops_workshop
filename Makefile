BROWSER = chromium # Not tested under any other browser.

# Browser flags for printing revealjs html slides to pdf.
BFLAGS = --headless --disable-gpu \
         --run-all-compositor-stages-befor-draw --virtual-time-budget=10000 \
         --print-to-pdf-no-header

SLIDES = $(shell find slides/ -name "*.Rmd")

.PHONY: clean stats slides manuscript man paper

all: clean stats slides manuscript

clean_stats:
	@echo "Cleaning stats directory..."
	-rm stats/data/clean_* stats/data/wrangled_* stats/results/*
	@printf "Done\n\n"

clean_slides:
	@echo "Cleaning slides directory..."
	-find ./slides -regex ".*\.html\|.*\.pdf" | xargs rm -f
	-rm -rf ./slides/git_demo
	@printf "Done\n\n"

clean_manuscript:
	@echo "Cleaning manuscript directory..."
	-rm manuscript/manuscript.docx manuscript/manuscript.pdf manuscript/manuscript.html
	@printf "Done\n\n"

clean: clean_stats clean_slides clean_manuscript

stats:
	-R -e 'setwd("stats/scripts"); source("run_all.R")'

slides: htmlslides pdfslides

%.html: %.Rmd
	R -e "setwd('$(<D)'); rmarkdown::render('$(<F)', output_file = '$(@F)')"

%.docx: %.Rmd
	R -e "setwd('$(<D)'); rmarkdown::render('$(<F)', output_file = '$(@F)')"

slides/%.pdf: slides/%.html
	${BROWSER} ${BFLAGS} file://$$(pwd)/$<?print-pdf --print-to-pdf=$@

slides/git.html: slides/git.Rmd
	mkdir -p slides/git_demo
	R -e "setwd('slides'); rmarkdown::render('git.Rmd', output_file = 'git.html')"

htmlslides:
	@make --no-print-directory $(SLIDES:.Rmd=.html)

pdfslides:
	@make --no-print-directory $(SLIDES:.Rmd=.pdf)

manuscript/%.pdf: manuscript/%.docx
	libreoffice --headless --convert-to pdf:writer_pdf_Export --outdir manuscript $<

# TODO: Use a better filter for html.
manuscript/%.html: manuscript/%.docx
	libreoffice --headless --convert-to html --outdir manuscript $<

manuscript:
	@make --no-print-directory manuscript/manuscript.docx
	@make --no-print-directory manuscript/manuscript.html
	@make --no-print-directory manuscript/manuscript.pdf

# Aliases for manuscript
man: manuscript
paper: manuscript
